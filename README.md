# Jaguar

[Windows 10 Enterprise LTSC](https://www.microsoft.com/en-us/evalcenter/download-windows-10-enterprise)

[tiny10 23H2 ISO](https://archive.org/details/tiny-10-23-h2)

[Acer Aspire One 722-0473](https://gadgetaz.com/Netbook/Acer_Aspire_One_722_AO722-0473--5446)

**Resolution: 1366 x 768**

* _DDR3 4 GB_
* _SODIMM_
* _1066 mHz_
* _204 pin_

